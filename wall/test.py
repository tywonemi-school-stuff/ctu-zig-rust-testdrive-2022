import socket

UDP_PORT = 1273
UDP_IP = "127.0.0.1"
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# XYRGB
i = 0
while True:
    if i == 255:
        i = 0
    else:
        i = 255
    for y in range(1000):
        for x in range(1000):
            message = bytes([x%256,int(x/256),y%256,int(y/256),y%256,x%256,i])
            sock.sendto(message, (UDP_IP, UDP_PORT))
print(message)
sock.close()