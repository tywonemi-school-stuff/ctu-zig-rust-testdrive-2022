extern crate minifb;
use minifb::{Key, Window, WindowOptions};
use std::net::UdpSocket;
use std::sync::mpsc;
use std::thread;
use std::time::{Duration, Instant};
const WIDTH: usize = 1000;
const HEIGHT: usize = 1000;
const FRAME_TIME: Duration = Duration::from_micros(16600);

fn main() -> std::io::Result<()> {
    {
        let (tx, rx) = mpsc::channel();
        let socket = UdpSocket::bind("127.0.0.1:1273")?;
        socket.set_read_timeout(Some(FRAME_TIME / 2))?; // 10 ms
        thread::spawn(move || loop {
            let mut msg_buf = [0; 10];
            let data = socket.recv_from(&mut msg_buf);
            let (amt, _) = match data {
                Ok((t1, t2)) => (t1, Some(t2)),
                Err(_) => (0, None),
            };
            if amt == 7 {
                tx.send(msg_buf).unwrap();
            }
        });
        let mut frame_buf: Vec<u32> = vec![0; WIDTH * HEIGHT];
        let mut window = Window::new("UDP pixel wall", WIDTH, HEIGHT, WindowOptions::default())
            .unwrap_or_else(|e| {
                panic!("{}", e);
            });
        // Limit to max ~60 fps update rate
        window.limit_update_rate(Some(FRAME_TIME));

        while window.is_open() && !window.is_key_down(Key::Escape) {
            let frame_start = Instant::now();
            while Instant::now().duration_since(frame_start) < FRAME_TIME / 2 {
                // render every 100k pixels unless socket timesout
                let received = rx.recv_timeout(FRAME_TIME / 2);
                match received {
                    Ok(msg) => {
                        let (x, y, r, g, b) = (
                            msg[0] as usize + 256 * msg[1] as usize,
                            msg[2] as usize + 256 * msg[3] as usize,
                            msg[4],
                            msg[5],
                            msg[6],
                        );
                        frame_buf[x + WIDTH * y] = 256 * (256 * r as u32 + g as u32) + b as u32;
                    }
                    Err(_) => break,
                };
            }
            // We unwrap here as we want this code to exit if it fails. Real applications may want to handle this in a different way
            window
                .update_with_buffer(&frame_buf, WIDTH, HEIGHT)
                .unwrap();
        }
    } // the socket is closed here
    Ok(())
}
